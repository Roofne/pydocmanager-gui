# PyDOCmanager-GUI

Logiciel offline permettant de rajouter des documents pour construire une bibliographie
Les documents importés sont classé dans un dossier.
Les annotations et les marqueurs sont contenu dans un fihcier JSON.
L'idee est d'annoter des documents pour avoir un classement

# Requirements

```bash
Python 3.11
PyQT   6.3
```

# TO DO

- Fichier de librairie
    - [x] Bouton creer collection
    - [x] Bouton ouvrir collection
    - [ ] Bouton d'ajout de document
        - [ ] creer un dossier et deplacer le document
        - [ ] maj collection
    - [ ] Bouton d'ajout de note
    - [ ] sauvegarde sur un cloud
        - [ ] Gdrive
        - [ ] Dropbox
- Visualization
    - [ ] liste des documents du fichier collection
    - [ ] infos du document
        - [ ] ID (nom dossier parent)
        - [ ] nom fichier
        - [ ] date
        - [ ] auteur
- Marqueurs
    - [ ] creation : nom/couleur
    - [ ] ajout => maj fichier librairie
- Analyse 
    - [ ] graphe chronologie (tableau kanban)
    - [ ] listing references
        - [ ] reference d'un document => Set
        - [ ] union des set + count
- Interface
    - [ ] dossier (1 document peut etre affecté à plusieurs dossier)

# References

## Librairie

https://doc.qt.io/qtforpython-6/examples/example_pdfwidgets_pdfviewer.html

## Porjet similaire

https://github.com/Archie3d/qpdf