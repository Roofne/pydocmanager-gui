import sys
from PyQt6.QtWidgets import QApplication, QWidget, QMainWindow, QLabel, QToolBar, QMenu, QFileDialog
from PyQt6.QtGui  import QAction
from PyQt6.QtCore import Qt

class Window(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("PyDOCmanager")
        self.setGeometry(100, 100, 600, 400)

        # Actions Widget
        self.addActionWidget()

        # Menu Bar
        self.addMenuBar()
        
        # Tool Bar


        # affichage des widgets
        self.show()
  
    # method for init
    def addActionWidget(self):
        self.newLibraryAction  = QAction("&New",  self)
        self.newLibraryAction.triggered.connect(self.onNewLibraryButtonClick)

        self.openLibraryAction = QAction("&Open", self)
        self.openLibraryAction.triggered.connect(self.onOpenLibraryButtonClick)


    def addMenuBar(self):
        menuBar = self.menuBar()

        # File menu
        fileMenu = QMenu("&File", self)
        fileMenu.addAction(self.newLibraryAction)
        fileMenu.addAction(self.openLibraryAction)
        menuBar.addMenu(fileMenu)

        # Edit Menu
        editMenu = QMenu("&Edit", self)
        menuBar.addMenu(editMenu)

        # Options Menu
        optionsMenu = QMenu("&Options", self)
        menuBar.addMenu(optionsMenu)

    # Event Handler
    def onNewLibraryButtonClick(self, s):
        self.newFileDialog()

    def onOpenLibraryButtonClick(self, s):
        self.openFileDialog()

    # C:\Users\vinte\OneDrive\Bureau\PROJET GIT\logiciel\reference-manager\mylib
    
    # File Dialog
    def newFileDialog(self):
        filepath, temp = QFileDialog.getSaveFileName(self, "New Library", "", "Library (*.txt)")  
        file = open(filepath, 'w')
        text = "Hello World"
        file.write(text)
        file.close()

    def openFileDialog(self):
        filepath, temp = QFileDialog().getOpenFileName(self, "Open Library", "", "Library (*.txt);; All Files (*);;")
        print(filepath)
        file = open(filepath, 'r')
        with file:
            text = file.read()
            print(text)
        file.close()

# PARTIE PRINCIPALE DU PROGRAMME
def main():
    app = QApplication(sys.argv)
    window = Window()
    sys.exit(app.exec())

if __name__ == '__main__':
   main()



"""
Tutorial

MenuBar + ToolBar :
    https://www.pythonguis.com/tutorials/pyqt6-actions-toolbars-menus/
    https://realpython.com/python-menus-toolbars/
"""